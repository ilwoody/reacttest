import Marty from "marty";

var ProjectConstants = Marty.createConstants([
  'RECEIVE_PROJECTS_LIST',
  'RECEIVE_ADD_PROJECT',
  'FAIL_ADD_PROJECT',
  'RECEIVE_DELETE_PROJECT',
  'FAIL_DELETE_PROJECT',
]);

export default ProjectConstants;
