import Marty from "marty";
import Router from "../router";

var NavigationActionCreators = Marty.createActionCreators({
  displayName: 'Navigation',
  navigateHome: function () {
    navigateTo('home');
  },
  navigateToProject: function (id) {
    navigateTo('project', { id: id });
  }
});

function navigateTo(route, params) {
  Router.transitionTo(route, params || {});
}

export default NavigationActionCreators;

