import Marty from "marty";
import ProjectConstants from "../constants/projectConstants";

var Actions = Marty.createActionCreators({
  receiveProjectsLists: ProjectConstants.RECEIVE_PROJECTS_LIST(function (projects) {
  	this.dispatch(projects);
  }),

  receiveAddProject: ProjectConstants.RECEIVE_ADD_PROJECT(function (localID, project) {
  	this.dispatch(localID, project);
  }),

  failAddProject: ProjectConstants.FAIL_ADD_PROJECT(function (localID) {
  	this.dispatch(localID);
  }),

  receiveDeleteProject: ProjectConstants.RECEIVE_DELETE_PROJECT(function (id) {
  	this.dispatch(id);
  }),  

  failDeleteProject: ProjectConstants.FAIL_DELETE_PROJECT(function (id) {
  	this.dispatch(id);
  }),  

});

export default Actions;

