require("6to5/polyfill");

import React from "react";
import Marty from "marty";
import Router from "./router";

window.React = React; // For React Developer Tools
window.Marty = Marty; // For Marty Developer Tools

if (process.env.NODE_ENV !== 'test') {
  Router.run((Handler, state) => {
    React.render(
      <Handler {...state.params} />,
      document.getElementById('app')
    );
  });
}

if (process.env.NODE_ENV === 'development') {
  Marty.Dispatcher.register((payload) => {
    if (payload.type === 'ACTION_FAILED') {
      console.error(payload.arguments[0].error.message);
      debugger;
    } else {
      console.info(payload.type, payload);
    }
  });
}
