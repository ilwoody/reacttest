import React from "react";
import cx from "react/lib/cx";
import ProjectStore from "../stores/projectStore";

var NewProject = React.createClass({

  getInitialState: function () {
  	return {open: false};
  },

  onOpen: function(event) {
  	this.setState({open: true});
  },

  onCreate: function(event) {
  	this.setState({open: false});
  	ProjectStore.addProject(this.state.value);
  },

  onCancel: function(event) {
  	this.setState({open: false});
  },

  handleChange: function(event) {
    this.setState({value: event.target.value});
  },  

  render() {
  	var formClass = cx({
  		'form': true,
  		'hidden': this.state.open == false
  	});

  	var createClass = cx({
  		'button--huge': true,
  		'hidden': this.state.open == true
  	});

    return (
      <div>
        <a className={createClass} onClick={this.onOpen}>Create new project</a>
        <form className={formClass}>
        	<fieldset>
	         	<legend>Create new project</legend>
	         	<div className="form__field">
	         		<label className="form__label">Project name</label>
		         	<input className="form__input--large" placeholder="Acme inc." type="text" onChange={this.handleChange} />
			        <div className="space--top-2">
				      <div className="space--both-2 or">
				        <div className="or__item">
				          <a className="button" href="#" onClick={this.onCreate}>Create</a>
				        </div>
				        <div className="or__item">        
				          <a className="button--alert" href="#" onClick={this.onCancel}>Cancel</a>
				        </div>
				      </div>
		    	    </div>
	         	</div>
	         </fieldset>
        </form>
      </div>
    );
  }
});

export default NewProject;
