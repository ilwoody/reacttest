import React from "react";
import Marty from "marty";
import ProjectStore from "../stores/projectStore";

var Project = React.createClass({

  onClick: function(event) {
    ProjectStore.deleteProject(this.props.data.id);
  },

  render() {
    var hiddenMe = (this.props.data.isDeleted == true) ? "hidden" : "";
    var updatingClass = (this.props.data.id == undefined) ? "" : "hidden";

    return (
      <div className={hiddenMe}>
        <div className="space--both-2 or">
          <div className="or__item">
            <p className="gamma">
            <span>{this.props.data.name}</span>
            <span className={updatingClass}> ..creating.. </span>
            </p>
          </div>
          <div className="or__item">        
            <a className="button--alert" href="#" onClick={this.onClick}>
              <i className="icon--cross"></i>
              <span>Delete</span>
            </a>
          </div>
        </div>
      </div>
    );
  }
});

export default Project;
