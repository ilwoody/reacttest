import React from "react";
import NewProject from "../components/newProject"
import ProjectList from "../components/projectList"

var Home = React.createClass({
  render() {
    return (
      <div className="wrap">
        <div className="alpha space--bottom-2" ref="title">Projects</div>
        <ProjectList />
        <div className="space--top-2"><NewProject/></div>
      </div>
    );
  }
});

export default Home;
