import React from "react";
import Marty from "marty";
import Project from "../components/project"
import ProjectStore from "../stores/projectStore";

var ProjectState = Marty.createStateMixin({
  listenTo: [ProjectStore],

  getState() {
    return {
      projects: ProjectStore.getAll()
    };
  }
});

var ProjectList = React.createClass({
  mixins: [ProjectState],

  render() {  	
    return this.state.projects.when({
      pending() {
        return <div className='loading'>Loading</div>;
      },
      failed(error) {
        return <div className='error'>{error.message}</div>;
      },
      done(projects) {
        if (projects.length == 0)
          return <div>No projects yet</div>;
        var ar = [];
        for (var key in projects) {
          ar.push(projects[key]);
        }
        return (
          <div>
            {ar.map(function (project) {
              return <Project data={project} key={project.id}/>;
            })
            }
          </div>
        );
      }
    });
  },
});

export default ProjectList;
