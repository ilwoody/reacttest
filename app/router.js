import React from "react";
import Router from "react-router";

import Home from "./components/home";
import Project from "./components/project";

var { Route } = Router;

var routes = [
  <Route name="home" path="/" handler={Home} />
];

export default Router.create({
  routes: routes,
  location: Router.HistoryLocation
});

