import Marty from "marty";
import ProjectConstants from "../constants/projectConstants";
import ProjectHttpStateSource from "../sources/projectHttpStateSource";

var ProjectStore = Marty.createStore({
  displayName: 'ProjectStore',
  counter: 0,

  handlers: {
    receiveProjectsList: ProjectConstants.RECEIVE_PROJECTS_LIST,
    receiveAddProject: ProjectConstants.RECEIVE_ADD_PROJECT,
    failAddProject: ProjectConstants.FAIL_ADD_PROJECT,
    receiveDeleteProject: ProjectConstants.RECEIVE_DELETE_PROJECT,
    failDeleteProject: ProjectConstants.FAIL_DELETE_PROJECT,
  },

  getInitialState() {
    return {
    };
  },

  getAll() {
    return this.fetch({
      id: "projectsGetAll",

      locally() {
        return this.state.projects;
      },

      remotely() {
        return ProjectHttpStateSource.getAll();
      }
    });
  },

  addProject(name) {
    var localID = 'addProject' + this.counter++; 

    this.state.projects[localID] = {name:name};
    this.hasChanged();

    return this.fetch({
      id: localID,

      locally() {
        return undefined; //this.state[id];
      },

      remotely() {
        return ProjectHttpStateSource.addProject(localID, name);
      }
    });
  },

  deleteProject(id) {
    this.state.projects[id].isDeleted = true;
    this.hasChanged();

    return this.fetch({
      id: id,

      locally() {
        return undefined; //this.state.projects[id];
      },

      remotely() {
        return ProjectHttpStateSource.deleteProject(id);
      },

      cacheError: false,
    });
  },

  receiveProjectsList(projects) {
    var np = {};
    for (var key in projects) {
      np[projects[key].id] = projects[key];
    }
    this.state.projects = np;
    this.hasChanged();
  },

  allProjects() {
    var ap = [];
    for (var key in this.state.projects) {
      ap.push(this.state.projects[key]);
    }  
    return ap;
  },

  receiveAddProject(localID, project) {
    delete this.state.projects[localID];
    this.state.projects[project.id] = project;
    this.hasChanged();
  },

  failAddProject(localID) {
    delete this.state.projects[localID];
    this.hasChanged();
  },

  receiveDeleteProject(id) {
    delete this.state.projects[id];
    this.hasChanged();
  },

  failDeleteProject(id) {
    delete this.state.projects[id].isDeleted;
    this.hasChanged();
  },

});

export default ProjectStore;
