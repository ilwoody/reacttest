import Marty from "marty";
import Actions from "../actions/projectSourceActionCreators";

var ProjectHttpStateSource = Marty.createStateSource({
  type: 'http',
  baseUrl: 'http://timetracks-api.herokuapp.com',

  getAll() {
  	return this.get('/projects')
  	  .then((res) => {
  	  	return Actions.receiveProjectsLists(res.body.projects);
  	  });
  },

  addProject(localID, name) {
  	return this.post({url:'/projects', body:{name:name}})
  	  .then((res) => {
  	  	return Actions.receiveAddProject(localID, res.body.projects[0]);
  	  })
  	  .catch((res) => {
  	  	return Actions.failAddProject(localID);
  	  });
  },

  deleteProject(id) {
  	return this.delete('/projects/' + id)
  	  .then((res) => {
  	  	return Actions.receiveDeleteProject(id);
  	  })
  	  .catch((res) => {
  	  	return Actions.failDeleteProject(id);
  	  });
  },



});

export default ProjectHttpStateSource;
